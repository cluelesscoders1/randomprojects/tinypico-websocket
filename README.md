[[_TOC_]]

# TinyPico Websocket

Using the TinyPico board, this program will allow someone to connect their board to a websocket server and respond. All directions are to install with the Android-Cli.

Note, some tweaking is needed because some documentation is old and some values need to be hard coded since the same headerfile name is in 2 places. 

# arduino-cli

So the documentation for this... sucks. Lets go have some fun and do it right. 

## Installation 

Install with a package manager in Linux or however windows does it. Just make sure you can type `arduiono-cli` and get something.

### Init the core

Very simple, you need to init the core first. Then add the board manager json for the TinyPico. Finally, install the board manager from the list. Those steps respectively are in the code block below. 

```bash
arduino-cli config init
arduino-cli config add board_manager.additional_urls https://dl.espressif.com/dl/package_esp32_index.json
arduino-cli core install esp32:esp32
```

Congrats! Now all ESP32 support is added the the arduino-cli.

### Library Installation
We will install 2 libraries now. One is fron the TinyPico developer. The other is for the WebSockets. 

```bash
arduino-cli lib install "TinyPICO Helper Library"
arduino-cli lib install WebSockets_Generic
```

Libraries are now installed. Now everything should work... but for me it didn't. 

### Compile Code

So now we're starting to get into normal territory. Assuming the `*.ino` file matches the name of the folder, it should
compile (hint, rename the `.ino` file or move it to another folder). Now if the `sketch.json` doesn't exist, now is 
when we tell the arduino cli to create it. 

Create `sketch.json` with:
```bash
arduino-cli board attach esp32:esp32:tinypico
```

Once that is done, you should be ready to build:
```bash
arduino-cli compile
```

Now if you're like me.... this is where things break. The code should work like the following, but didn't for me. 

```cpp
#include WiFi/src/WiFi.h>
#include WiFi/src/WiFiMulti.h>
#include WiFiClientSecure/src/WiFiClientSecure.h>

#include <WebSocketsClient.h>
```

What fixed it for me for the multi file complaint. 
```cpp
#include </home/MyUserName/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WiFi/src/WiFi.h>
#include </home/MyUserName/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WiFi/src/WiFiMulti.h>
#include </home/MyUserName/.arduino15/packages/esp32/hardware/esp32/1.0.6/libraries/WiFiClientSecure/src/WiFiClientSecure.h>

#include <WebSocketsClient_Generic.h>
```

If the `WebSocketClient.h` complains, slap generic on it and it fixes that error as shown above. 

### Upload Code 

So this gets weird, but on linux it's simple to follow these steps. 
1. Plug board in
1. Look in `/dev/ttyUSBX` <-- Change X with a number, I'm betting on 0. 
1. Now comes the fun part, `sudo chmod 777 /dev/ttyUSBX` again, check the number. 
1. Finally upload code with `arduino-cli upload -p /dev/ttyUSBX`. Remember, `X` marks the spot... for swapping with a number

Now that you've done that, congrats! Time to actually fix the code so it works for you. Time to go back into the `.ino` file.


# Code Updates 

So assuming the code compiles, find the lines and edit them (mostly in the bottom). 

Router information, around line 96. 
```cpp
// Put your name of the router (2.4GHz name) and WPA password here
WiFiMulti.addAP("ssid_name_here", "wpa_password_here");
```

If you want to connect to NOT BuzzChat, chat this on line 104
```cpp
// server address, port and URL, set to BuzzChat right now. 
webSocket.begin("68.82.134.116", 8003, "/ws");
```

After these changes you should be ready to go. Good luck! 
